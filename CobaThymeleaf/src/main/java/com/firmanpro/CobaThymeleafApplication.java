package com.firmanpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CobaThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(CobaThymeleafApplication.class, args);
	}
}
