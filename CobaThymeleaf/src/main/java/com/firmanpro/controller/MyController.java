package com.firmanpro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyController {

	@RequestMapping(value = "/")
	public String homePage(Model model) {
		model.addAttribute("tagbro1","Berhasil");
		model.addAttribute("tagbro2","Sukses");
		return "home";
	}
	
}
